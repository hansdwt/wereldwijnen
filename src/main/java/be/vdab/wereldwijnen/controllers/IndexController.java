// Hans Dewitte
package be.vdab.wereldwijnen.controllers;

import be.vdab.wereldwijnen.domain.BestelLijn;
import be.vdab.wereldwijnen.entities.BestelBon;
import be.vdab.wereldwijnen.forms.IdEnAantal;
import be.vdab.wereldwijnen.services.LandService;
import be.vdab.wereldwijnen.services.SoortService;
import be.vdab.wereldwijnen.services.WijnService;
import be.vdab.wereldwijnen.sessions.Mandje;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import static be.vdab.wereldwijnen.forms.Bestelwijze.AFHALEN;

@Controller
@RequestMapping("/")
public class IndexController {
    private final LandService landService;
    private final SoortService soortService;
    private final WijnService wijnService;
    private final Mandje mandje;

    public IndexController(LandService landService, SoortService soortService, WijnService wijnService, Mandje mandje) {
        this.landService = landService;
        this.soortService = soortService;
        this.wijnService = wijnService;
        this.mandje = mandje;
    }

    @GetMapping
    ModelAndView getIndexPagina() {
        var modelAndView = new ModelAndView("index")
                .addObject("landIds", landService.findAllIds());
        if (! mandje.isLeeg()) {
            modelAndView.addObject(mandje)
                    .addObject(getBestelLijnen())
                    .addObject("totaal", getTotaal())
                    .addObject(new BestelBon(null, null, null, null, (short) 1000, AFHALEN));
        }
        return modelAndView;
    }

    @GetMapping("land/{id}")
    ModelAndView getLandPagina(@PathVariable long id) {
        var mogelijkLand = landService.findById(id);
        if (mogelijkLand.isEmpty()) {
            return new ModelAndView("404");
        } else {
            var land = mogelijkLand.get();
            return getIndexPagina()
                    .addObject("land", land);
        }
    }

    @GetMapping("land/{landId}/soort/{soortId}")
    ModelAndView getSoortPagina(@PathVariable long landId, @PathVariable long soortId) {
        var mogelijkeSoort = soortService.findById(soortId);
        if (mogelijkeSoort.isEmpty()) {
            return new ModelAndView("404");
        } else {
            var soort = mogelijkeSoort.get();
            return getLandPagina(landId)
                    .addObject("soort", soort);
        }
    }

    @GetMapping("land/{landId}/soort/{soortId}/wijn/{wijnId}")
    ModelAndView getSoortToevoegenPagina(@PathVariable long landId, @PathVariable long soortId, @PathVariable long wijnId) {
        var idEnAantal = new IdEnAantal(wijnId, 1);
        return getSoortToevoegenPaginaMetForm(landId, soortId, wijnId, idEnAantal);
    }

    @PostMapping("land/{landId}/soort/{soortId}/wijn/{wijnId}")
    ModelAndView postWijnInMandje(@PathVariable long landId, @PathVariable long soortId, @PathVariable long wijnId, @Valid IdEnAantal idEnAantal, Errors errors) {
        if (errors.hasErrors()) {
            System.err.println("form heeft fouten");
        } else {
            mandje.voegToe(idEnAantal);
        }
        return getSoortToevoegenPaginaMetForm(landId, soortId, wijnId, idEnAantal);
    }

    private ModelAndView getSoortToevoegenPaginaMetForm(long landId, long soortId, long wijnId, IdEnAantal idEnAantal) {
        var mogelijkeWijn = wijnService.findById(wijnId);
        if (mogelijkeWijn.isEmpty()) {
            return new ModelAndView("404");
        } else {
            return getSoortPagina(landId, soortId)
                    .addObject("wijn", mogelijkeWijn.get())
                    .addObject("idEnAantal", idEnAantal);
        }
    }

    private List<BestelLijn> getBestelLijnen() {
        return mandje.getIdsEnAantallen()
                .stream()
                .map(this::maakLijn)
                .collect(Collectors.toList());
    }

    private BestelLijn maakLijn(IdEnAantal idEnAantal) {
        return BestelLijn.from(idEnAantal, wijnService);
    }

    private BigDecimal getTotaal() {
        return getBestelLijnen().stream()
                .map(BestelLijn::getSubTotaal)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}
