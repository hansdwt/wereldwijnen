package be.vdab.wereldwijnen.domain;

import be.vdab.wereldwijnen.entities.Wijn;
import be.vdab.wereldwijnen.forms.IdEnAantal;
import be.vdab.wereldwijnen.services.WijnService;

import java.math.BigDecimal;
import java.util.Objects;

public class BestelLijn {
    private final Wijn wijn;
    private final int aantal;

    public BestelLijn(Wijn wijn, int aantal) {
        this.wijn = wijn;
        this.aantal = aantal;
    }

    public static BestelLijn from(IdEnAantal idEnAantal, WijnService service) {
        int aantal = idEnAantal.getAantal();
        var wijn = service.findById(idEnAantal.getId())
                .orElseThrow();
        return new BestelLijn(wijn, aantal);
    }

    public long getId() {
        return wijn.getId();
    }

    public String getWijn() {
        return wijn.toString();
    }

    public BigDecimal getPrijs() {
        return wijn.getPrijs();
    }

    public int getAantal() {
        return aantal;
    }

    public BigDecimal getSubTotaal() {
        return wijn.getPrijs()
                .multiply(BigDecimal.valueOf(aantal));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BestelLijn that = (BestelLijn) o;
        return aantal == that.aantal &&
                wijn.equals(that.wijn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(wijn, aantal);
    }
}
