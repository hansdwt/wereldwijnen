// Hans Dewitte
package be.vdab.wereldwijnen.sessions;

import be.vdab.wereldwijnen.forms.IdEnAantal;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Component
@SessionScope
public class Mandje implements Serializable {
    private static final long serialVersionUID = "Mandje".hashCode();

    private final List<IdEnAantal> idsEnAantallen;

    public Mandje() {
        this.idsEnAantallen = new ArrayList<>();
    }

    public void voegToe(IdEnAantal lijn) {
        idsEnAantallen.add(lijn);
    }

    public List<IdEnAantal> getIdsEnAantallen() {
        return List.copyOf(idsEnAantallen);
    }

    public boolean isLeeg() {
        return idsEnAantallen.isEmpty();
    }

    public int getAantalArtikels() {
        return idsEnAantallen.size();
    }

    public void maakLeeg() {
        idsEnAantallen.clear();
    }
}
