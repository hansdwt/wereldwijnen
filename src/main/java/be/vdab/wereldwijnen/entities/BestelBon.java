// Hans Dewitte
package be.vdab.wereldwijnen.entities;

import be.vdab.wereldwijnen.forms.Bestelwijze;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Objects;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "bestelbonnen")
public class BestelBon {
    @Id @GeneratedValue(strategy = IDENTITY) private long id;
    @NotBlank
    private String naam, straat, huisnummer, gemeente;
    @Min(1000) @Max(9999)
    private short postCode;
    @NotNull
    private Bestelwijze bestelwijze;

    public BestelBon(
            String naam,
            String straat,
            String huisnummer,
            String gemeente,
            short postcode,
            Bestelwijze bestelwijze
    ) {
        this.naam = naam;
        this.straat = straat;
        this.huisnummer = huisnummer;
        this.gemeente = gemeente;
        this.postCode = postcode;
        this.bestelwijze = bestelwijze;
    }

    public String getNaam() {
        return naam;
    }
    public String getStraat() {
        return straat;
    }
    public String getHuisnummer() {
        return huisnummer;
    }
    public String getGemeente() {
        return gemeente;
    }
    public short getPostCode() {
        return postCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BestelBon bestelBon = (BestelBon) o;
        return postCode == bestelBon.postCode &&
                naam.equals(bestelBon.naam) &&
                straat.equals(bestelBon.straat) &&
                huisnummer.equals(bestelBon.huisnummer) &&
                gemeente.equals(bestelBon.gemeente);
    }

    @Override
    public int hashCode() {
        return Objects.hash(naam, straat, huisnummer, gemeente, postCode);
    }
}
