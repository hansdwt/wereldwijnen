// Hans Dewitte
package be.vdab.wereldwijnen.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "soorten")
public class Soort implements Serializable {
    private static final long serialVersionUID = 1L;
    private String naam;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private long id;

    @ManyToOne(fetch = LAZY, optional = false)
    @JoinColumn(name = "landId")
    private Land land;

    @OneToMany(mappedBy = "soort")
    private final Set<Wijn> wijnen = new HashSet<>();

    public Soort(String naam, Land land) {
        this.naam = naam;
        this.land = land;
        land.addSoort(this);
    }
    protected Soort() {}

    public String getNaam() {
        return naam;
    }

    public Land getLand() {
        return land;
    }

    public long getId() {
        return id;
    }

    void addWijn(Wijn wijn) {
        wijnen.add(wijn);
    }

    public Set<Wijn> getWijnen() {
        return Collections.unmodifiableSet(wijnen);
    }

    @Override
    public String toString() {
        return this.getNaam();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Soort soort = (Soort) o;
        return Objects.equals(naam, soort.naam);
    }

    @Override
    public int hashCode() {
        return Objects.hash(naam);
    }
}
