// Hans Dewitte
package be.vdab.wereldwijnen.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "landen")
public class Land implements Serializable {
    private static final long serialVersionUID = 1L;
    private String naam;

    @OneToMany(mappedBy = "land")
    private final Set<Soort> soorten = new HashSet<>();

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private long id;

    public Land(String naam) {
        this.naam = naam;
    }
    protected Land() {}

    public String getNaam() {
        return naam;
    }

    void addSoort(Soort soort) {
        soorten.add(soort);
    }

    public Set<Soort> getSoorten() {
        return Collections.unmodifiableSet(soorten);
    }

    public long getId() {
        return id;
    }

    @Override
    public String toString() {
        return this.getNaam();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Land land = (Land) o;
        return naam.equalsIgnoreCase(land.naam);
    }

    @Override
    public int hashCode() {
        return Objects.hash(naam);
    }
}
