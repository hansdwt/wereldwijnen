// Hans Dewitte
package be.vdab.wereldwijnen.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "wijnen")
public class Wijn implements Serializable {
    private static final long serialVersionUID = 1L;
    private int jaar;
    private byte beoordeling;
    private BigDecimal prijs;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private long id;

    @ManyToOne(fetch = LAZY, optional = false)
    @JoinColumn(name = "soortId")
    private Soort soort;

    public Wijn(Soort soort, int jaar) {
        this.soort = soort;
        this.jaar = jaar;
        soort.addWijn(this);
    }
    protected Wijn() {}

    public Soort getSoort() {
        return soort;
    }

    public int getJaar() {
        return jaar;
    }

    public BigDecimal getPrijs() {
        return prijs;
    }

    public long getId() {
        return id;
    }

    public byte getBeoordeling() {
        return beoordeling;
    }

    @Override
    public String toString() {
        return String.valueOf(getJaar());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Wijn wijn = (Wijn) o;
        return jaar == wijn.jaar;
    }

    @Override
    public int hashCode() {
        return Objects.hash(jaar);
    }
}
