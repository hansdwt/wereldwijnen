// Hans Dewitte
package be.vdab.wereldwijnen.forms;

import javax.validation.constraints.Positive;
import java.util.Objects;

public class IdEnAantal {
    @Positive
    private final int aantal;
    @Positive
    private final long id;

    public IdEnAantal(long id, int aantal) {
        this.id = id;
        this.aantal = aantal;
    }

    public long getId() {
        return id;
    }

    public int getAantal() {
        return aantal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IdEnAantal that = (IdEnAantal) o;
        return aantal == that.aantal &&
                id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(aantal, id);
    }
}
