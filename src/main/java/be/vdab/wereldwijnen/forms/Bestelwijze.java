// Hans Dewitte
package be.vdab.wereldwijnen.forms;

public enum Bestelwijze {
    AFHALEN, OPSTUREN
}
