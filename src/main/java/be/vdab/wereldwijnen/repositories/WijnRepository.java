// Hans Dewitte
package be.vdab.wereldwijnen.repositories;

import be.vdab.wereldwijnen.entities.Wijn;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WijnRepository extends JpaRepository<Wijn, Long> {}
