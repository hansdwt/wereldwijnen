// Hans Dewitte
package be.vdab.wereldwijnen.repositories;

import be.vdab.wereldwijnen.entities.Soort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SoortRepository extends JpaRepository<Soort, Long> {}
