// Hans Dewitte
package be.vdab.wereldwijnen.repositories;

import be.vdab.wereldwijnen.entities.Land;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LandRepository extends JpaRepository<Land, Long> {

    @Query("select l.id from Land l")
    List<Long> findAllIds();

}
