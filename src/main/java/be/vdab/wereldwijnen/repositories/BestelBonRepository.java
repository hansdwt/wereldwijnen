// Hans Dewitte
package be.vdab.wereldwijnen.repositories;

import be.vdab.wereldwijnen.entities.BestelBon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BestelBonRepository extends JpaRepository<BestelBon, Long> {}
