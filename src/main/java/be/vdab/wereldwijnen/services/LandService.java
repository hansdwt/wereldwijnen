// Hans Dewitte
package be.vdab.wereldwijnen.services;

import be.vdab.wereldwijnen.entities.Land;
import be.vdab.wereldwijnen.repositories.LandRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
public class LandService {
    private final LandRepository repository;

    public LandService(LandRepository repository) {
        this.repository = repository;
    }

    public Optional<Land> findById(long id) {
        return repository.findById(id);
    }

    public List<Long> findAllIds() {
        return repository.findAllIds();
    }
}
