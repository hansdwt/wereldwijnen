// Hans Dewitte
package be.vdab.wereldwijnen.services;

import be.vdab.wereldwijnen.entities.Soort;
import be.vdab.wereldwijnen.repositories.SoortRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional(readOnly = true)
public class SoortService {
    private final SoortRepository repository;

    public SoortService(SoortRepository repository) {
        this.repository = repository;
    }

    public Optional<Soort> findById(long id) {
        return repository.findById(id);
    }
}
