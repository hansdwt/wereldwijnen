// Hans Dewitte
package be.vdab.wereldwijnen.services;

import be.vdab.wereldwijnen.entities.Wijn;
import be.vdab.wereldwijnen.repositories.WijnRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional(readOnly = true)
public class WijnService {
    private WijnRepository repository;

    public WijnService(WijnRepository repository) {
        this.repository = repository;
    }

    public Optional<Wijn> findById(long wijnId) {
        return repository.findById(wijnId);
    }
}
