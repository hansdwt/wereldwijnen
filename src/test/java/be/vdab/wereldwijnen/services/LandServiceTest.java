// Hans Dewitte
package be.vdab.wereldwijnen.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LandServiceTest {
    @Autowired private LandService service;
    private final static String LAND_NAAM = "Australië";
    private final static long LAND_ID = 1;


    @Test
    public void findById() {
        var mogelijkLand = service.findById(LAND_ID);
        var naam = mogelijkLand.orElseThrow().getNaam();
        assertEquals(LAND_NAAM, naam);
    }

    @Test
    public void findIds() {
        var ids = service.findAllIds();
        assertTrue(ids.contains(LAND_ID));
    }
}