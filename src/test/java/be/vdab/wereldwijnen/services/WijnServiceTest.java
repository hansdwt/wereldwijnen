// Hans Dewitte
package be.vdab.wereldwijnen.services;

import be.vdab.wereldwijnen.entities.Land;
import be.vdab.wereldwijnen.entities.Soort;
import be.vdab.wereldwijnen.entities.Wijn;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WijnServiceTest {
    @Autowired private WijnService service;
    private static final Land LAND = new Land("Australië");
    private static final Soort SOORT = new Soort("Chardonnay", LAND);
    private final static Wijn WIJN = new Wijn(SOORT, 1982);
    private final static long WIJN_ID = 1;


    @Test
//    @Transactional
    public void findById() {
        var mogelijkeWijn = service.findById(WIJN_ID);
//        assertEquals(
//                WIJN.getSoort().getLand(),
//                mogelijkeWijn.orElseThrow().getSoort().getLand()
//        );
        assertEquals(
                WIJN,
                mogelijkeWijn.orElseThrow()
        );
    }

}