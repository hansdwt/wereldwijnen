// Hans Dewitte
package be.vdab.wereldwijnen.domain;

import be.vdab.wereldwijnen.entities.Land;
import be.vdab.wereldwijnen.entities.Soort;
import be.vdab.wereldwijnen.entities.Wijn;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class AssociatieTest {
    private Land land;
    private Soort soort;
    private Wijn wijn;

    @Before
    public void setUp() {
        land = new Land("België");
        soort = new Soort("Geuze", land);
        wijn = new Wijn(soort, 2013);
    }

    @Test
    public void viaLandKanIkAanSoort() {
        assertTrue(land.getSoorten().contains(soort));
    }

    @Test
    public void viaSoortKanIkAanLand() {
        assertEquals(land, soort.getLand());
    }

    @Test
    public void viaSoortKanIkAanWijn() {
        assertTrue(soort.getWijnen().contains(wijn));
    }

    @Test
    public void viaWijnKanIkAanSoort() {
        assertEquals(soort, wijn.getSoort());
    }


}
