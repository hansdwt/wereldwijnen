// Hans Dewitte
package be.vdab.wereldwijnen.forms;

import org.junit.Before;
import org.junit.Test;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class IdEnAantalTest {
    private Validator validator;

    @Before
    public void before() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void aantalOk() {
        assertTrue(
                validator.validateValue(
                        IdEnAantal.class, "aantal", 1
                ).isEmpty()
        );
    }

    @Test
    public void aantalMoetMinstensNulZijn() {
        assertFalse(validator.validateValue(
                IdEnAantal.class, "aantal", -1
        ).isEmpty()
        );
    }
    @Test
    public void idOk() {
        assertTrue(
                validator.validateValue(
                        IdEnAantal.class, "id", 1L
                ).isEmpty()
        );
    }

    @Test
    public void idMoetMinstensNulZijn() {
        assertFalse(validator.validateValue(
                IdEnAantal.class, "id", -1L
        ).isEmpty()
        );
    }
}