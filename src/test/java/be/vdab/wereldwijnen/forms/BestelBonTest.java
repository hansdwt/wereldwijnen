// Hans Dewitte
package be.vdab.wereldwijnen.forms;

import be.vdab.wereldwijnen.entities.BestelBon;
import org.junit.Before;
import org.junit.Test;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BestelBonTest {
    private Validator validator;

    @Before
    public void before() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void naamOk() {
        assertTrue(
                validator.validateValue(
                        BestelBon.class, "naam", "Nemo"
                ).isEmpty()
        );
    }

    @Test
    public void naamMoetIngevuldZijn() {
        assertFalse(
                validator.validateValue(
                        BestelBon.class, "naam", null
                ).isEmpty()
        );
    }
    @Test
    public void straatOk() {
        assertTrue(
                validator.validateValue(
                        BestelBon.class, "straat", "Nemo"
                ).isEmpty()
        );
    }

    @Test
    public void straatMoetIngevuldZijn() {
        assertFalse(
                validator.validateValue(
                        BestelBon.class, "straat", null
                ).isEmpty()
        );
    }
    @Test
    public void huisnummerOk() {
        assertTrue(
                validator.validateValue(
                        BestelBon.class, "huisnummer", "Nemo"
                ).isEmpty()
        );
    }

    @Test
    public void huisnummerMoetIngevuldZijn() {
        assertFalse(
                validator.validateValue(
                        BestelBon.class, "huisnummer", null
                ).isEmpty()
        );
    }
    @Test
    public void gemeenteOk() {
        assertTrue(
                validator.validateValue(
                        BestelBon.class, "gemeente", "Nemo"
                ).isEmpty()
        );
    }

    @Test
    public void gemeenteMoetIngevuldZijn() {
        assertFalse(
                validator.validateValue(
                        BestelBon.class, "gemeente", null
                ).isEmpty()
        );
    }

    @Test
    public void postcodeOk() {
        assertTrue(
                validator.validateValue(
                        BestelBon.class, "postcode", 3001
                ).isEmpty()
        );
    }

    @Test
    public void postcodeMoetMinstens1000Zijn() {
        assertFalse(
                validator.validateValue(
                        BestelBon.class, "postcode", 999
                ).isEmpty()
        );
    }

    @Test
    public void postcodeMoetMaximaal9999Zijn() {
        assertFalse(
                validator.validateValue(
                        BestelBon.class, "postcode", 10_000
                ).isEmpty()
        );
    }
}